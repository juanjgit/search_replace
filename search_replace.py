import argparse
import os
import re

def search_and_replace(search_regex, replace_pattern, root_dir, exclude=[]):

  if exclude is None:
    exclude = []

  # Walk through the directory tree
  for root, dirs, files in os.walk(root_dir):
    # Exclude specified directories
    dirs[:] = [d for d in dirs if d not in exclude]

    # Iterate through the files
    for file in files:
      file_path = os.path.join(root, file)
      try:
        
        with open(file_path, 'r', encoding='utf-8') as f:
          # Read the file contents
          file_contents = f.read()

        # Search and replace using the regular expression
        replaced_contents = search_regex.sub(replace_pattern, file_contents)

        # Write the modified contents back to the file
        with open(file_path, 'w', encoding='utf-8') as f:
          f.write(replaced_contents)

        # Print the name of the file if it was modified
        if file_contents != replaced_contents:
          print(f'Modified file: {file_path}')

      except UnicodeDecodeError as e:
        print(f'Failed to process file {file_path}: {e}')

if __name__ == '__main__':
  # Parse command-line arguments
  parser = argparse.ArgumentParser(description='Search and replace in files using regular expressions')
  parser.add_argument('search_pattern', help='The regular expression pattern to search for', type=re.compile)
  parser.add_argument('replace_pattern', help='The replacement string')
  parser.add_argument('root_dir', help='The root directory to search')
  parser.add_argument('--exclude', help='A list of directories to exclude', nargs='+')
  args = parser.parse_args()

  # Call the search_and_replace function with the parsed arguments
  search_and_replace(args.search_pattern, args.replace_pattern, args.root_dir, exclude=args.exclude)
