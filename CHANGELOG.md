# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.2](https://gitlab.com/juanjgit/search_replace/compare/v1.1.1...v1.1.2) (2023-12-05)


### Bug Fixes

* too slow 2 ([20f0dfb](https://gitlab.com/juanjgit/search_replace/commit/20f0dfbbe2dd38a4190c37f7f4417530878ad2b8))

### [1.1.1](https://gitlab.com/juanjgit/search_replace/compare/v1.1.0...v1.1.1) (2023-12-05)


### Bug Fixes

* too slow ([bd3279a](https://gitlab.com/juanjgit/search_replace/commit/bd3279a8ec057caf0867c87881f5d7e9ebef8924))

## [1.1.0](https://gitlab.com/juanjgit/search_replace/compare/v1.0.1...v1.1.0) (2023-12-05)


### Features

* I missed to upload the actual script! ([8129f5e](https://gitlab.com/juanjgit/search_replace/commit/8129f5e05c59260f8a1275c3c783e3bc7144b078))

### [1.0.1](https://gitlab.com/juanjgit/search_replace/compare/v1.0.0...v1.0.1) (2023-12-05)


### Bug Fixes

* English translation ([0e4dbee](https://gitlab.com/juanjgit/search_replace/commit/0e4dbeeb6663099dec268fb4951cce2daa3ffb71))

## 1.0.0 (2023-12-05)


### Features

* initial commit ([3d13718](https://gitlab.com/juanjgit/search_replace/commit/3d13718e184d6e95a0dada299e4436e841b7e555))
