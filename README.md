# Search and Replace Script

This Python script allows you to search and replace text in files using regular expressions. It's useful for making bulk changes across multiple files within a directory and its subdirectories.

## Requirements

- Python 3
- `chardet` library

To install `chardet`, run the following command:

```bash
pip install chardet
```

## Usage

To use the script, you need to provide a search pattern, a replacement string, and a root directory. Optionally, you can exclude specific directories.

```bash
python search_replace.py 'search_pattern' 'replace_pattern' 'root_dir' --exclude dir1 dir2
```

- `search_pattern`: Regular expression pattern to search for in the files.
- `replace_pattern`: Text string that will replace the found matches.
- `root_dir`: The root directory where the search will begin.
- `--exclude`: Optional list of directories to exclude from the search.

## Example

If you want to replace all instances of "old_text" with "new_text" in the files under the directory "/path/to/directory", excluding the directories "dir1" and "dir2", you would use the following command:

```bash
python search_replace.py 'old_text' 'new_text' '/path/to/directory' --exclude dir1 dir2
```

## Error Handling

The script handles character encoding errors and continues processing the next files if it encounters a problem. If it cannot read a file due to an encoding error, it will print an error message indicating the file that caused the problem.

## Contributions

Contributions to the script are welcome. If you have suggestions or improvements, feel free to create a pull request or open an issue in the repository.

## License

This script is distributed under the MIT license.
